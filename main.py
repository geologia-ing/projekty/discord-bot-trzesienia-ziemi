import discord
from discord.ext import commands, tasks
import os
import json
from urllib.request import urlopen
from datetime import datetime

client = discord.Client()
url = (
    'https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.geojson'
)
number_of_events = 0
something_new = False


def get_json_data(url):
    response = urlopen(url)
    data = response.read().decode("utf-8")
    return json.loads(data)


def get_event():
    global number_of_events, something_new
    Fcol = get_json_data(url)
    number = Fcol['metadata']['count']

    if number > number_of_events:
        number_of_events = number
        something_new = True
        event = "New Earthquake detected! "

        event += "Magnitude: " + str(
            Fcol['features'][0]['properties']['mag']) + " "
        event += "Place: " + str(
            Fcol['features'][0]['properties']['place']) + "; "
        ms = Fcol['features'][0]['properties']['time']
        event += "Time: " + str(datetime.fromtimestamp(ms / 1000)) + " "
        event += "Coordinates: " + str(
            Fcol['features'][0]['geometry']['coordinates']) + " "
        event += "Tsunami: " + str(
            Fcol['features'][0]['properties']['tsunami']) + " "
    else:
        something_new = False
        event = "No New Earthquakes currently detected "

    return event


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('hello'):
        await message.channel.send('Hello!')

    if message.content.startswith('$event'):
        await message.channel.send('```' + get_event() + '```')


@tasks.loop(seconds=300)
async def send_message():
  global something_new
  await client.wait_until_ready()
  channel = client.get_channel(int(985518826328297502))
  response = get_event()
  print(response)
  if something_new:
    await channel.send('```' + response + '```')


send_message.start()
client.run(os.environ['TOKEN'])