# Discord bot - trzęsienia Ziemi



## Co to za projekt?

Projekt ten to bot do aplikacji discord, który informuje o odbywających się obecnie trzęsieniach ziemi, ich lokalizacji, czasie oraz magnitudzie. 


## Jak go uruchomić?

- W miejscu TOKEN wpisać token swojego bota ze strony Discord Development Program. 
- Dołączyć bota na swój serwer discord.
- W funkcji async def w zmiennej channel = client.get_channel wpisać ID kanału na ID kanału, na jakim ma się wykonywać.
- Uruchomić bota. 


## Cały kod napisany jest samodzielnie, więc nie potrzeba licencji!
